/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"
#include "syscall.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#define DEBUG

int main( int argc, char *argv[] )
{
	FILE *f;
	struct IF_ID_buffer if_id;
	struct ID_EX_buffer id_ex;
	struct EX_MEM_buffer ex_mem;
	struct MEM_WB_buffer mem_wb;
	int i;
    size_t count;

	// Set ins ex counter
	cpu_count.lwCount = 0;
	cpu_count.swCount = 0;
	cpu_count.beqCount = 0;
	cpu_count.bneCount = 0;
	cpu_count.jalCount = 0;
	cpu_count.jalrCount = 0;
	cpu_count.auipcCount = 0;
	cpu_count.ecallCount = 0;
	cpu_count.addCount = 0;
	cpu_count.subCount = 0;
	cpu_count.sllCount = 0;
	cpu_count.sltCount = 0;
	cpu_count.xorCount =  0;
	cpu_count.srlCount = 0;
	cpu_count.sraCount = 0;
	cpu_count.orCount = 0;
	cpu_count.andCount = 0;
	cpu_count.addiCount = 0;
	cpu_count.slliCount = 0;
	cpu_count.sltiCount = 0;
	cpu_count.xoriCount = 0;
	cpu_count.srliCount = 0;
	cpu_count.sraiCount = 0;
	cpu_count.oriCount = 0;
	cpu_count.andiCount = 0;
	cpu_count.cycleCount = 0;

  /* Set the PC */
	cpu_ctx.PC = 0x00400000;


	/* Initialize register to 0 */
	for ( i = 0; i < 32; i++ ) {
		cpu_ctx.GPR[i] = 0;
	}

  /* Set the SP */
  cpu_ctx.GPR[29] = 0x10002000;

	/* Read memory from the input file */
	f = fopen(argv[1], "r");
	assert(f);

  count = fread(data_memory, sizeof(uint32_t), 1024, f);
  assert(count == 1024);

  count = fread(instruction_memory, sizeof(uint32_t), 1024, f);

  printf("Instruction Memory: %u \n", instruction_memory[0]);
  assert(count == 1024);
  fclose(f);
  
  // Process data memory
  // Process Instruction Memeory
	unsigned  mask;
	mask = ((1 << 5) - 1) << 15;
	unsigned int test = instruction_memory[0] & mask;
	printf("\nRead Reg 1: %d", test);


	int counter = 0;

	if_id.instruction = instruction_memory[counter];
	if_id.pc = cpu_ctx.PC;

	

	while(1) {
#if defined(DEBUG)
		printf("FETCH from PC=%x\n", cpu_ctx.PC);

#endif

		printf("fetch start\n");
		fetch( &if_id );
		printf("fetch end\n");
				printf("decode start\n");

		decode( &if_id, &id_ex );

		printf("decode end\n");

		printf("ex start\n");

		execute( &id_ex, &ex_mem );
		printf("ex end\n");
		printf("mem start\n");

		memory( &ex_mem, &mem_wb );
				printf("mem end\n");
		printf("wb start\n");

		writeback( &mem_wb );
				printf("wb end\n");


		if ( cpu_ctx.PC == 0 ) break;
	}
	printf("Total number of lw instructions = %d\n", cpu_count.lwCount);
	printf("Total number of sw instructions = %d\n", cpu_count.swCount);
	printf("Total number of beq instructions = %d\n", cpu_count.beqCount);
	printf("Total number of bne instructions = %d\n", cpu_count.bneCount);
	printf("Total number of jal instructions = %d\n", cpu_count.jalCount);
	printf("Total number of jalr instructions = %d\n", cpu_count.jalrCount);
	printf("Total number of auipc instructions = %d\n", cpu_count.auipcCount);
	printf("Total number of ecall instructions = %d\n", cpu_count.ecallCount);
	printf("Total number of add instructions = %d\n", cpu_count.addCount);
	printf("Total number of sub instructions = %d\n", cpu_count.subCount);
	printf("Total number of sll instructions = %d\n", cpu_count.sllCount);
	printf("Total number of slt instructions = %d\n", cpu_count.sltCount);
	printf("Total number of xor instructions = %d\n", cpu_count.xorCount);
	printf("Total number of srl instructions = %d\n", cpu_count.srlCount);
	printf("Total number of sra instructions = %d\n", cpu_count.sraCount);
	printf("Total number of or instructions = %d\n", cpu_count.orCount);
	printf("Total number of and instructions = %d\n", cpu_count.andCount);
	printf("Total number of addi instructions = %d\n", cpu_count.addiCount);
	printf("Total number of slli instructions = %d\n", cpu_count.slliCount);
	printf("Total number of slti instructions = %d\n", cpu_count.sltiCount);
	printf("Total number of xori instructions = %d\n", cpu_count.xoriCount);
	printf("Total number of srli instructions = %d\n", cpu_count.srliCount);
	printf("Total number of srai instructions = %d\n", cpu_count.sraiCount);
	printf("Total number of ori instructions = %d\n", cpu_count.oriCount);
	printf("Total number of andi instructions = %d\n", cpu_count.andiCount);
	printf("Total number of Cycles = %d", cpu_count.cycleCount);
	return 0;
}


