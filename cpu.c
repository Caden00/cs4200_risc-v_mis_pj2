/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct cpu_context cpu_ctx;
struct cpu_counter cpu_count;

int fetch( struct IF_ID_buffer *out )
{
	uint32_t offset = 0x00400000;
	out->pc = cpu_ctx.PC;
	out->instruction = (out->pc - offset) / 4;
	out->next_pc = cpu_ctx.PC + 4;
	cpu_count.cycleCount++;
	return 0;
}

int decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out )
{
	// Pass program counter
	out->pc = in->pc;
	out->next_pc = in->next_pc;

	// Extract opcode
	unsigned  mask;
	mask = (1 << 7) - 1;
	out->opcode = in->instruction & mask;

	// Extract readReg 1
	mask = ((1 << 5) - 1) << 15;
	out->readReg1 = in->instruction & mask;

	// Extract readReg 2
	mask = ((1 << 5) - 1) << 20;
	out->readReg2 = in->instruction & mask;

	// Extract write Reg
	mask = ((1 << 5) -1) << 7;
	out->writeReg = in->instruction & mask;

	// Save immediate generation value
	out->immGen = in->instruction;

	// Control
	out->insType = control(in->instruction);
	out->instruction = in->instruction;
	return 0;
}

int execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out )
{
	out->instruction = in->instruction;
	out->readReg1 = in->readReg1;
	out->readReg2 = in->readReg2;
	out->writeReg = in->writeReg;
	out->pc = in->pc;
	out->next_pc = in->next_pc;
	out->insType = in->insType;

	int aluOp = 0;
	// Call to ALU control
	switch (in->insType){
		case 1:
			// register alu op
			aluOp = aluControl(in->instruction, 1);
			break;
		case 2: 
			// immediate alu up
			aluOp = aluControl(in->instruction, 2);
			break;
		case 3:
			// lw
			out->memOp = 'lw';
			break;
		case 4:
			// sw
			out->memOp = 'sw';
			break;
		case 5:
			// branches
			branchTaken = branchCalc(in->instruction, in);
			if (branchTaken == 1){
				immVal = immGen(in->instruction, 'sb');
				out->next_pc = in->pc + 4 * immVal;
			}
			break;
		case 6:
			// jalr
			out->next_pc = in->pc + cpu_ctx.GPR[in->readReg1];
			cpu_count.jalrCount++;
			break;
		case 7:
			// jal
			immVal = immGen(in->instruction, 'uj');
			out->next_pc = (in->pc + 4) + (immVal * 4);
			cpu_count.jalCount++;
			break;
		case 8:
			// auipc
			immVal = immGen(in->instruction, 'u');
			out->next_pc = in->pc + (immVal << 12);
			cpu_count.auipcCount++;
			break;
		case 9: 
			//ecall
			_sysCallInt = cpu_ctx.GPR[17]; // a7 = 0x17 = register 17
			syscall(_sysCallInt, cpu_ctx);
			cpu_count.ecallCount++;
			break;
		default:
			printf("Operation not supported\n");
	}
	if (aluOp != 0 && aluOp != 1){
		out->returnRegVal = alu(aluOp, in);
	}

	cpu_ctx.PC = out->next_pc;

	return 0;
}

int memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out )
{
	if(in->insType == 3 || in->insType == 4){
		//LW
		if(in->insType == 3){
			immVal = immGen(in->instruction, "i");
			out->returnRegVal = data_memory[in->readReg1 + immVal];
			cpu_count.lwCount++;
		}
		//SW
		else if (in->insType == 4){
			immVal = immGen(in->instruction, "s");
			data_memory[in->readReg1 + immVal] = cpu_ctx.GPR[in->readReg2];
			cpu_count.swCount++;
		}
		else{
			printf("In the else statement\n");
		}
	}
	return 0;
}

int writeback( struct MEM_WB_buffer *in )
{	
	cpu_ctx.GPR[in->writeReg] = in->returnRegVal;
	return 0;
}

int control(uint32_t instruction){

	// TODO: Turn these in enums if we have time
	// returns 1 meaning register alu operation
	// 2 = alu op with an immediate (ex. addi)
	// 3 = lw
	// 4 = sw
	// 5 = branches
	// 6 = jalr
	// 7 = jal
	// 8 = auipc
	// 9 = ecall

	uint32_t opcode = instruction & ((1 << 7) - 1);
	switch (opcode){
		case 0x33:
			return 1;
			break;
		case 0x13:
			return 2;
			break;
		case 0x03:
			return 3;
			break;
		case 0x23:
			return 4;
			break;
		case 0x63:
			return 5;
			break;
		case 0x67:
			return 6;
			break;
		case 0x6F:
			return 7;
			break;
		case 0x17:
			return 8;
			break;
		case 0x72:
			return 9;
			break;
		default:
			return -1; //opcode not supported
	};
	/************* POSSIBLY USE IF EASY WAY DOES NOT WORK *************
	// if R type (add, sub, and, or)
	controller->aluSource = 0;
	controller->memToReg = 0;
	controller->regWrite = 1;
	controller->memRead = 0;
	controller->memWrite = 0;
	controller->branch = 0;
	controller->aluOp = 1;
	*/
/*
	// if lw
	controller->aluSource = 1;
	controller->memToReg = 1;
	controller->regWrite = 1;
	controller->memRead = 1;
	controller->memWrite = 0;
	controller->branch = 0;
	controller->aluOp = 0;

	// if sw
	controller->aluSource = 1;
	controller->memToReg = 0; // x in book
	controller->regWrite = 0;
	controller->memRead = 0;
	controller->memWrite = 1;
	controller->branch = 0;
	controller->aluOp = 0; // might need to implement aluOp1 and aluOp2 pg 278

	// if beq
	controller->aluSource = 0;
	controller->memToReg = 0; // x in book
	controller->regWrite = 0;
	controller->memRead = 0;
	controller->memWrite = 0;
	controller->branch = 1;
	controller->aluOp = 0; // might need to implement aluOp1 and aluOp2 pg 278
*/
}

int aluControl(uint32_t instruction, int type) {

	// TODO: Turn into enums if time is available
	// 1 = sub
	// 2 = add
	// 3 = sll;
	// 4 = slt
	// 5 = xor
	// 6 = srl
	// 7 = sra
	// 8 = or
	// 9 = and
	// 10 = addi
	// 11 = slli
	// 12 = slti
	// 13 = xori
	// 14 = srli
	// 15 = srai
	// 16 = ori
	// 17 = andi
	// 18 = beq
	// 19 = bne


	// get funct3 & 7 values
	int funct3 = instruction & ((1 << 3) - 1 << 12);
	int funct7 = instruction & ((1 << 7) - 1 << 25);
	if (type == 1){
		// add in breaks and default. default should be -1
		switch(funct3){
				case 0x0:
					if(funct7 == 0x20)
						return 1; // alu should subtract
					else
						return 2; // add
					break;
				case 0x1:
					return 3; // sll
					break;
				case 0x2:
					return 4; //slt
					break;
				case 0x4:
					return 5; // xor
					break;
				case 0x5:
						if (funct7 == 0)
							return 6; // srl
						else
							return 7; // sra
						break;
				case 0x6:
					return 8; // or
					break;
				case 0x7:
					return 9; // and
					break;
				default:
					return -1;
		};
	}
	else //if (type == 2) // alu w/ immediate
	// add breaks and default. default = -1
		switch(funct3){
			case 0x0:
				return 10;
				break;
			case 0x1:
				return 11;
				break;
			case 0x2:
				return 12;
				break;
			case 0x4:
				return 13;
				break;
			case 0x5:
				if (funct7 == 0)
					return 14;
				else
					return 15;
				break;
			case 0x6:
				return 16;
				break;
			case 0x7:
				return 17;
				break;
			default:
				return -1;
		};
	}


int alu(struct ID_EX_buffer *in, int aluOp) {

	switch (aluOp){
		case 1: // subtract
			cpu_count.subCount++;
			return cpu_ctx.GPR[in->readReg2] - cpu_ctx.GPR[in->readReg1];
			break;
		case 2: // add
			cpu_count.addCount++;
			return cpu_ctx.GPR[in->readReg2] + cpu_ctx.GPR[in->readReg1];
			break;
		case 3: // sll
			cpu_count.sllCount++;
			return cpu_ctx.GPR[in->readReg2] << cpu_ctx.GPR[in->readReg1];
			break;
		case 4: // slt
			if (cpu_ctx.GPR[in->readReg2] > cpu_ctx.GPR[in->readReg1]){
				cpu_count.sltCount++;
				return 1;
			}
			else{ 
				cpu_count.sltCount++;
				return 0;
			}
			break;
		case 5: // xor
			cpu_count.xorCount++;
			return cpu_ctx.GPR[in->readReg2] ^ cpu_ctx.GPR[in->readReg1];
			break;
		case 6: // srl
			cpu_count.srlCount++;
			return cpu_ctx.GPR[in->readReg1] >> cpu_ctx.GPR[in->readReg2];
			break;
		case 7: // sra
			cpu_count.sraCount++;
			return cpu_ctx.GPR[in->readReg1] >> cpu_ctx.GPR[in->readReg2];
			break;
		case 8: // or
			cpu_count.orCount++;
			return cpu_ctx.GPR[in->readReg2] | cpu_ctx.GPR[in->readReg1];
			break;
		case 9: // and
			cpu_count.andCount++;
			return cpu_ctx.GPR[in->readReg2] & cpu_ctx.GPR[in->readReg1];
			break;

		// immediate arith
		uint32_t immediate = in->instruction & ((1 << 12) - 1) << 20;

		case 10: // addi
			cpu_count.addiCount++;
			return cpu_ctx.GPR[in->readReg1] + immediate;
			break;
		case 11: // slli
			cpu_count.slliCount++;
			return cpu_ctx.GPR[in->readReg1] << immediate;
			break;
		case 12: // slti
			if (cpu_ctx.GPR[in->readReg1] < immediate){
				cpu_count.sltiCount++;
				return 1;
			}
			else{
				cpu_count.sltiCount++;
				return 0;
			}
			break;
		case 13: // xori
			cpu_count.xoriCount++;
			return cpu_ctx.GPR[in->readReg1] ^ immediate;
			break;
		case 14: // srli
			cpu_count.srliCount++;
			return cpu_ctx.GPR[in->readReg1] >> immediate;
			break;
		case 15: // srai
			cpu_count.sraiCount++;
			return cpu_ctx.GPR[in->readReg1] >> immediate;
			break;
		case 16: // ori
			cpu_count.oriCount++;
			return cpu_ctx.GPR[in->readReg1] | immediate;
			break;
		case 17: // andi
			cpu_count.andiCount++;
			return cpu_ctx.GPR[in->readReg1] & immediate;
			break;
		default:
			return -1;
	}
}

int branchCalc(uint32_t instruction, struct ID_EX_buffer *in){

	uint32_t opcode = instruction & ((1 << 7) - 1);
	int funct3 = instruction & ((1 << 3) - 1 << 12);

	if (funct3 == 0){ //beq
		cpu_count.beqCount++;
		if (cpu_ctx.GPR[in->readReg2] == cpu_ctx.GPR[in->readReg1])
			return 1; // branch
		else 
			return 0; // do not branch
	}
	else // bne
		cpu_count.bneCount++;
		if (cpu_ctx.GPR[in->readReg2] != cpu_ctx.GPR[in->readReg1])
			return 1; // branch
		else 
			return 0; // do not branch

}

uint32_t immGen(uint32_t instruction, char * format){

	char* immValArr;
    int insArr[32]; 
    int i = 0;
	uint32_t immVal; 
	uint32_t tempInstruction;

		// getting int parameter as a binary array
		while (instruction > 0) {
			insArr[i] = tempInstruction % 2;
			tempInstruction = tempInstruction / 2;
			i++;
		}
	if (strcmp(format, 'sb')){

		immValArr[0] = 0;
		immValArr[1] = insArr[8];
		immValArr[2] = insArr[9];
		immValArr[3] = insArr[10];
		immValArr[4] = insArr[11];
		immValArr[5] = insArr[25];
		immValArr[6] = insArr[26];
		immValArr[7] = insArr[27];
		immValArr[8] = insArr[28];
		immValArr[9] = insArr[29];
		immValArr[10] = insArr[30];
		immValArr[11] = insArr[7];
		immValArr[12] = insArr[31];

		char * nullptr;
		immVal = strtoul(immValArr, &nullptr, 2);
	}
	else if(strcmp(format, 'uj')){

		immValArr[0] = insArr[12];
		immValArr[1] = insArr[13];
		immValArr[2] = insArr[14];
		immValArr[3] = insArr[15];
		immValArr[4] = insArr[16];
		immValArr[5] = insArr[17];
		immValArr[6] = insArr[18];
		immValArr[7] = insArr[19];
		immValArr[8] = insArr[11];
		immValArr[9] = insArr[1];
		immValArr[10] = insArr[2];
		immValArr[11] = insArr[3];
		immValArr[12] = insArr[4];
		immValArr[13] = insArr[5];
		immValArr[14] = insArr[6];
		immValArr[15] = insArr[7];
		immValArr[16] = insArr[8];
		immValArr[17] = insArr[9];
		immValArr[18] = insArr[10];
		immValArr[19] = insArr[20];

		char * nullptr;
		immVal = strtoul(immValArr, &nullptr, 2);
	}
	else if(strcmp(format, 'u')){
		immVal = instruction >> 11;
	}
	else if(strcmp(format, 'i')){
		immVal = instruction >> 19;
	}
	else if(strcmp(format, 's')){

		immValArr[0] = insArr[7];
		immValArr[1] = insArr[8];
		immValArr[2] = insArr[9];
		immValArr[3] = insArr[10];
		immValArr[4] = insArr[11];
		immValArr[5] = insArr[25];
		immValArr[6] = insArr[26];
		immValArr[7] = insArr[27];
		immValArr[8] = insArr[28];
		immValArr[9] = insArr[29];
		immValArr[10] = insArr[30];
		immValArr[11] = insArr[31];

		char * nullptr;
		immVal = strtoul(immValArr, &nullptr, 2);
	}
	else
		return 0;

	return immVal;	
}

