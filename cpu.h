/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>

struct cpu_context {
	uint32_t PC;
	uint32_t GPR[32];
};

struct cpu_counter
{
	int lwCount;
	int swCount;
	int beqCount;
	int bneCount;
	int jalCount;
	int jalrCount;
	int auipcCount;
	int ecallCount;
	int addCount;
	int subCount;
	int sllCount;
	int sltCount;
	int xorCount;
	int srlCount;
	int sraCount;
	int orCount;
	int andCount;
	int addiCount;
	int slliCount;
	int sltiCount;
	int xoriCount;
	int srliCount;
	int sraiCount;
	int oriCount;
	int andiCount;
	int cycleCount;
};

extern struct cpu_counter cpu_count;
extern struct cpu_context cpu_ctx;

struct IF_ID_buffer {
	uint32_t instruction;
	uint32_t pc;
	uint32_t next_pc;
};

struct ID_EX_buffer {

	uint32_t opcode;
	uint32_t readReg1;
	uint32_t readReg2;
	uint32_t writeReg;
	uint32_t immGen;
	uint32_t insType;
	uint32_t instruction;
	uint32_t pc;
	uint32_t next_pc;
};

struct EX_MEM_buffer {
	int returnRegVal;
	uint32_t pc;
	uint32_t next_pc;
	char * memOp;
	uint32_t instruction;
	uint32_t readReg1;
	uint32_t readReg2;
	uint32_t writeReg;
	uint32_t insType;
};

struct MEM_WB_buffer {

	int returnRegVal;
	uint32_t pc;
	uint32_t next_pc;
	char * memOp;
	uint32_t instruction;
	uint32_t readReg1;
	uint32_t readReg2;
	uint32_t writeReg;

	};

struct Control {
	uint32_t branch;
	uint32_t memRead;
	uint32_t memToReg;
	uint32_t aluOp;
	uint32_t memWrite;
	uint32_t aluSource;
	uint32_t regWrite;
};

uint32_t immVal;
uint32_t immGen(uint32_t, char *format);
int branchTaken;
int _sysCallInt;

int fetch( struct IF_ID_buffer *out );
int decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out );
int execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out );
int memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out );
int writeback( struct MEM_WB_buffer *in );
int control(uint32_t);
int alu(struct ID_EX_buffer *in, int);
int AluControl(uint32_t, int);
int branchCalc(uint32_t instruction, struct ID_EX_buffer *in);
